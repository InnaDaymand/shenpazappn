'''
Created on May 17, 2020

@author: Inna Daymand
'''
import tkinter as tk
from tkinter import filedialog
from tkinter import simpledialog
import os
from SANNewFurnaceModel import SANNewFurnaceModel
from SANNewUser import SANNewUser

class SANDatabaseManage(object):

    def __init__(self, master, dbconnect):
        self.master=master
        self.master.title("Manage Database")
        self.topframeBSD = tk.Frame(self.master)
        self.topframeBSD.pack(fill=tk.X)
        self.topBSDLabel=tk.Label(self.topframeBSD, text='בסד')
        self.topBSDLabel.pack(side=tk.RIGHT)
        self.topframe = tk.Frame(self.master)
        self.topframe.pack(fill=tk.X, after=self.topframeBSD)
                 
        self.cellarframe=tk.Frame(self.master)
        self.cellarframe.pack(side=tk.BOTTOM)
        self.cellarframe.pack(fill=tk.X)
        self.cellarframe["height"]=10

        self.bottomframe=tk.Frame(self.master)
        self.bottomframe.pack(side=tk.BOTTOM)
        self.bottomframe.pack(fill=tk.X, after=self.cellarframe)
                
        self.bottomMessageframe=tk.Frame(self.master)
        self.bottomMessageframe.pack(side=tk.BOTTOM)
        self.bottomMessageframe.pack(fill=tk.X, after=self.bottomframe)
        self.bottomMessageLabel=tk.Label(self.bottomMessageframe)
        self.bottomMessageLabel.pack(fill=tk.X)
                
        self.butBack=tk.Button(self.topframe, text="Back", command=self.closeWindow)
        self.butBack.pack(side=tk.RIGHT)
         
        self.dbconnection=dbconnect
        self.makeWindow()


    def makeWindow(self):
        self.AddStartupLogoImageframe = tk.Frame(self.master)
        self.AddStartupLogoImageframe.pack(fill=tk.X)
        butAddStartupLogoImage=tk.Button(self.AddStartupLogoImageframe, text="Add Image StartupLogo (jpeg)", command=self.addStartupLogoImage)
        butAddStartupLogoImage.pack(side=tk.LEFT)
        butDeleteStartupLogoImage=tk.Button(self.AddStartupLogoImageframe, text="Delete Image StartupLogo (jpeg)", command=self.deleteStartupLogoImage)
        butDeleteStartupLogoImage.pack(side=tk.RIGHT)
        self.AddClientLogoImageframe = tk.Frame(self.master)
        self.AddClientLogoImageframe.pack(fill=tk.X)
        butAddClientLogoImage=tk.Button(self.AddClientLogoImageframe, text="Add Image ClientLogo (jpeg)", command=self.addClientLogoImage)
        butAddClientLogoImage.pack(side=tk.LEFT)
        butDeleteClientLogoImage=tk.Button(self.AddClientLogoImageframe, text="Delete Image ClientLogo (jpeg)", command=self.deleteClientLogoImage)
        butDeleteClientLogoImage.pack(side=tk.RIGHT)
        self.AddManufactureLogoImageframe = tk.Frame(self.master)
        self.AddManufactureLogoImageframe.pack(fill=tk.X)
        butAddManufactureLogoImage=tk.Button(self.AddManufactureLogoImageframe, text="Add Image ManufactureLogo (jpeg)", command=self.addManufactureLogoImage)
        butAddManufactureLogoImage.pack(side=tk.LEFT)
        butDeleteManufactureLogoImage=tk.Button(self.AddManufactureLogoImageframe, text="Delete Image ManufactureLogo (jpeg)", command=self.deleteManufactureLogoImage)
        butDeleteManufactureLogoImage.pack(side=tk.RIGHT)
        self.AddManualframe = tk.Frame(self.master)
        self.AddManualframe.pack(fill=tk.X)
        butAddManual=tk.Button(self.AddManualframe, text="Add Manual (PDF)", command=self.addManual)
        butAddManual.pack(side=tk.LEFT)
        butDeleteManual=tk.Button(self.AddManualframe, text="Delete Manual (PDF)", command=self.deleteManual)
        butDeleteManual.pack(side=tk.RIGHT)
        self.AddVIAProgramsframe = tk.Frame(self.master)
        self.AddVIAProgramsframe.pack(fill=tk.X)
        butAddViaPrograms=tk.Button(self.AddVIAProgramsframe, text="Add VIA Programs (CSV)", command=self.addVIAPrograms)
        butAddViaPrograms.pack(side=tk.LEFT)
        butDeleteViaPrograms=tk.Button(self.AddVIAProgramsframe, text="Delete VIA Programs (CSV)", command=self.deleteVIAPrograms)
        butDeleteViaPrograms.pack(side=tk.RIGHT)
        self.AddFurnaceModelframe = tk.Frame(self.master)
        self.AddFurnaceModelframe.pack(fill=tk.X)
        butAddFurnaceModel=tk.Button(self.AddFurnaceModelframe, text="Add Furnace Model", command=self.addFurnaceModel)
        butAddFurnaceModel.pack(side=tk.LEFT)
        butDeleteFurnaceModel=tk.Button(self.AddFurnaceModelframe, text="Delete Furnace Model", command=self.deleteFurnaceModel)
        butDeleteFurnaceModel.pack(side=tk.RIGHT)

        self.AddUserframe = tk.Frame(self.master)
        self.AddUserframe.pack(fill=tk.X)
        butAddUser=tk.Button(self.AddUserframe, text="Add User", command=self.addUser)
        butAddUser.pack(side=tk.LEFT)
        butDeleteUser=tk.Button(self.AddUserframe, text="Delete User", command=self.deleteUser)
        butDeleteUser.pack(side=tk.RIGHT)

    def addStartupLogoImage(self):
        typeimage=1
        self.addImage(typeimage)
        self.master.focus()

    def addClientLogoImage(self):
        typeimage=2
        self.addImage(typeimage)
        self.master.focus()

    def addManufactureLogoImage(self):
        typeimage=3
        self.addImage(typeimage)
        self.master.focus()

    def deleteStartupLogoImage(self):
        typeimage=1
        self.deleteImage(typeimage)
        self.master.focus()

    def deleteClientLogoImage(self):
        typeimage=2
        self.deleteImage(typeimage)
        self.master.focus()

    def deleteManufactureLogoImage(self):
        typeimage=3
        self.deleteImage(typeimage)
        self.master.focus()

    def deleteImage(self, typeimage):
        self.bottomMessageLabel['fg']='green'
        self.bottomMessageLabel['text']=''
        self.SearchWords=simpledialog.askstring("Input", "Type please search word(s)",
                                parent=self.master)
        if(self.SearchWords == ''):
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='Search result is empty\r\n'
            self.master.focus()
            return
        try:
            cursor=self.dbconnection.cursor()
                        
            sqlDeleteQuery="delete from Images where IDTypeImage="+str(typeimage)+" and NameImage like '%"+self.SearchWords+"%'"
                        
            
            cursor.execute(sqlDeleteQuery)
            
            self.dbconnection.commit()
            self.bottomMessageLabel['text']='Images was deleted successfully!!!\r\n'
        except Exception as e:
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='Images deleting was fail!!! Exception is '+str(e)+'\r\n'
            
    def addImage(self, typeimage):
        self.bottomMessageLabel['fg']='green'
        self.bottomMessageLabel['text']=''
        self.ImageFileName=filedialog.askopenfilename(initialdir='/', title="Select image file ", filetypes=(("png files", "*.png"), ("jpeg files", "*.jpeg, *.jpg")))
        if(self.ImageFileName == ''):
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='Image is not selected!!!\r\n'
            self.master.focus()
            return
        try:
            cursor=self.dbconnection.cursor()
            
            imagedata=self.convertToBinaryData(self.ImageFileName)
            
            sqlInsertQuery="insert into Images(IDTypeImage, NameImage, ImageData) values (%s, %s, %s)"
            
            filename=os.path.basename(self.ImageFileName)
            filename=filename.replace(" ", "-")
            
            insert_data=(typeimage, filename, imagedata)
            
            cursor.execute(sqlInsertQuery, insert_data)
            
            self.dbconnection.commit()
            self.bottomMessageLabel['text']='Image was saved successfully!!!\r\n'
        except Exception as e:
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='Image saving was fail!!! Exception is '+str(e)+'\r\n'
            
            
    
        
    def addManual(self):
        self.bottomMessageLabel['fg']='green'
        self.bottomMessageLabel['text']=''
        self.ManualFileName=filedialog.askopenfilename(initialdir='/', title="Select pdf file ")
        if(self.ManualFileName == ''):
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='File Manual is not selected!!!\r\n'
            self.master.focus()
            return
        try:
            cursor=self.dbconnection.cursor()
            
            manualdata=self.convertToBinaryData(self.ManualFileName)
            
            sqlInsertQuery="insert into Manuals(NameManual, ManualData) values (%s, %s)"
            
            filename=os.path.basename(self.ManualFileName)
            filename=filename.replace(" ", "-")
            
            insert_data=(filename, manualdata)
            
            cursor.execute(sqlInsertQuery, insert_data)
            
            self.dbconnection.commit()
            self.bottomMessageLabel['text']='Manual file was saved successfully!!!\r\n'
        except Exception as e:
            self.bottomMessageLabel['text']='Manual file saving was fail!!! Exception is '+str(e)+'\r\n'
            
        self.master.focus()

    def deleteManual(self):
        self.bottomMessageLabel['fg']='green'
        self.bottomMessageLabel['text']=''
        self.SearchWords=simpledialog.askstring("Input", "Type please search word(s)",
                                parent=self.master)
        if(self.SearchWords == ''):
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='Search result is empty\r\n'
            self.master.focus()
            return
        try:
            cursor=self.dbconnection.cursor()
                        
            sqlDeleteQuery="delete from  Manuals where NameManual like '%"+self.SearchWords+"%'"
            
            cursor.execute(sqlDeleteQuery)
            
            self.dbconnection.commit()
            self.bottomMessageLabel['text']='Manual file was deleted successfully!!!\r\n'
        except Exception as e:
            self.bottomMessageLabel['text']='Manual file deleting was fail!!! Exception is '+str(e)+'\r\n'
            
        self.master.focus()

    def addVIAPrograms(self):
        self.bottomMessageLabel['fg']='green'
        self.bottomMessageLabel['text']=''
        self.VIAProgramFileName=filedialog.askopenfilename(initialdir='/', title="Select csv file ")
        if(self.VIAProgramFileName == ''):
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='File VIAProgram is not selected!!!\r\n'
            self.master.focus()
            return
        
        try:
            cursor=self.dbconnection.cursor()
            
            viaprogramdata=self.convertToBinaryData(self.VIAProgramFileName)
            
            sqlInsertQuery="insert into VIAPrograms(NameVIAProgram, VIAProgramData) values (%s, %s)"
            
            filename=os.path.basename(self.VIAProgramFileName)
            filename=filename.replace(" ", "-")
            
            insert_data=(filename, viaprogramdata)
            
            cursor.execute(sqlInsertQuery, insert_data)
            
            self.dbconnection.commit()
            self.bottomMessageLabel['text']='CSV file was saved successfully!!!\r\n'
        except Exception as e:
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='CSV file saving was fail!!! Exception is '+str(e)+'\r\n'

        self.master.focus()
    
    def deleteVIAPrograms(self):
        self.bottomMessageLabel['fg']='green'
        self.bottomMessageLabel['text']=''
        self.SearchWords=simpledialog.askstring("Input", "Type please search word(s)",
                                parent=self.master)
        if(self.SearchWords == ''):
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='Search result is empty\r\n'
            self.master.focus()
            return
          
        try:
            cursor=self.dbconnection.cursor()
            
            sqlDeleteQuery="delete from VIAPrograms where NameVIAProgram like '%"+self.SearchWords+"%'"
            
            cursor.execute(sqlDeleteQuery)
            
            self.dbconnection.commit()
            self.bottomMessageLabel['text']='CSV file was deleted successfully!!!\r\n'
        except Exception as e:
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='CSV file deleting was fail!!! Exception is '+str(e)+'\r\n'

        self.master.focus()

    def addFurnaceModel(self):
        self.bottomMessageLabel['text']=''
        self.newWindow = tk.Toplevel(self.master)
        self.newWindow.geometry("600x200")
        self.app = SANNewFurnaceModel(self.newWindow, self.dbconnection)

    def deleteFurnaceModel(self):
        self.bottomMessageLabel['fg']='green'
        self.bottomMessageLabel['text']=''
        self.SearchWords=simpledialog.askstring("Input", "Type please search word(s)",
                                parent=self.master)
        if(self.SearchWords == ''):
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='Search result is empty\r\n'
            self.master.focus()
            return
          
        try:
            cursor=self.dbconnection.cursor()
            
            sqlDeleteQuery="delete from FurnaceModels where FurnaceModelName like '%"+self.SearchWords+"%'"
            
            cursor.execute(sqlDeleteQuery)
            
            self.dbconnection.commit()
            self.bottomMessageLabel['text']='Furnace models was deleted successfully!!!\r\n'
        except Exception as e:
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='Furnace models deleting was fail!!! Exception is '+str(e)+'\r\n'

        self.master.focus()

    def addUser(self):
        self.bottomMessageLabel['text']=''
        self.newWindow = tk.Toplevel(self.master)
        self.newWindow.geometry("600x200")
        self.app = SANNewUser(self.newWindow, self.dbconnection)

    def deleteUser(self):
        self.bottomMessageLabel['fg']='green'
        self.bottomMessageLabel['text']=''
        self.SearchWords=simpledialog.askstring("Input", "Type please search word(s)",
                                parent=self.master)
        if(self.SearchWords == ''):
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='Search result is empty\r\n'
            self.master.focus()
            return
          
        try:
            cursor=self.dbconnection.cursor()
            
            sqlDeleteQuery="delete from Users where NameUser like '%"+self.SearchWords+"%'"
            
            cursor.execute(sqlDeleteQuery)
            
            self.dbconnection.commit()
            self.bottomMessageLabel['text']='Users was deleted successfully!!!\r\n'
        except Exception as e:
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='Users deleting was fail!!! Exception is '+str(e)+'\r\n'

        self.master.focus()

    def closeWindow(self):
        self.master.destroy()
    
    def convertToBinaryData(self, filename):
        # Convert digital data to binary format
        with open(filename, 'rb') as file:
            binaryData = file.read()
        return binaryData