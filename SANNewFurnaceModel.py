'''
Created on 23 May 2020

@author: idaymand
'''

import tkinter as tk
import tkinter.ttk as ttk

class SANNewFurnaceModel(object):
    def __init__(self, master, dbconnect):
        self.master=master
        self.master.title("Add New Furnace Model")
        self.topframeBSD = tk.Frame(self.master)
        self.topframeBSD.pack(fill=tk.X)
        self.topBSDLabel=tk.Label(self.topframeBSD, text='בסד')
        self.topBSDLabel.pack(side=tk.RIGHT)
        self.topframe = tk.Frame(self.master)
        self.topframe.pack(fill=tk.X, after=self.topframeBSD)
                 
                 
        self.cellarframe=tk.Frame(self.master)
        self.cellarframe.pack(side=tk.BOTTOM)
        self.cellarframe.pack(fill=tk.X)
        self.cellarframe["height"]=10

        self.bottomframe=tk.Frame(self.master)
        self.bottomframe.pack(side=tk.BOTTOM)
        self.bottomframe.pack(fill=tk.X, after=self.cellarframe)
                
        self.butBack=tk.Button(self.topframe, text="Back", command=self.closeWindow)
        self.butBack.pack(side=tk.RIGHT)

        self.bottomMessageframe=tk.Frame(self.master)
        self.bottomMessageframe.pack(side=tk.BOTTOM)
        self.bottomMessageframe.pack(fill=tk.X, after=self.bottomframe)
        self.bottomMessageLabel=tk.Label(self.bottomMessageframe)
        self.bottomMessageLabel.pack(fill=tk.X)
        
        self.buttonSave=ttk.Button(self.bottomframe, text="Save", command=self.insertFurnaceModel)
        self.buttonSave.pack(side=tk.LEFT, padx=10)
         
        self.buttonCancel=ttk.Button(self.bottomframe, text="Cancel", command=self.closeWindow)
        self.buttonCancel.pack(side=tk.RIGHT, padx=10)

        self.dbconnection=dbconnect
        self.makeWindow()
        
    def makeWindow(self):
        self.FurnaceModelframe = tk.Frame(self.master)
        self.FurnaceModelframe.pack(fill=tk.X)
        self.FurnaceModelE = ttk.Entry(self.FurnaceModelframe)
        self.FurnaceModelE['width']=30
        self.FurnaceModelE.pack(side=tk.RIGHT)
        self.FurnaceModelLabel=tk.Label(self.FurnaceModelframe, text=" New Furnace Model")
        self.FurnaceModelLabel.pack(side=tk.LEFT)

    def closeWindow(self):
        self.master.destroy()
        
    def insertFurnaceModel(self):
        self.bottomMessageLabel['fg']='green'
        try:
            cursor=self.dbconnection.cursor()
            NameFurnaceModel=self.FurnaceModelE.get()
    
            insert_query='insert into FurnaceModels (FurnaceModelName) values("'+NameFurnaceModel+'")'
            
            cursor.execute(insert_query)
            
            self.dbconnection.commit()
            self.bottomMessageLabel['text']='Furnace Model was added successfully!!!'
        except Exception as e:
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='Something is wrong! Exception is ' +str(e)        