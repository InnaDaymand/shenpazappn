'''
Created on 10 Jun 2020

@author: Inna Daymand
'''
import tkinter as tk
from tkinter import ttk
class SANNewUser(object):
    '''
    classdocs
    '''


    def __init__(self, master, dbconnect):
        self.master=master
        self.master.title("Add New User")
        self.topframeBSD = tk.Frame(self.master)
        self.topframeBSD.pack(fill=tk.X)
        self.topBSDLabel=tk.Label(self.topframeBSD, text='בסד')
        self.topBSDLabel.pack(side=tk.RIGHT)
        self.topframe = tk.Frame(self.master)
        self.topframe.pack(fill=tk.X, after=self.topframeBSD)
                 
        self.cellarframe=tk.Frame(self.master)
        self.cellarframe.pack(side=tk.BOTTOM)
        self.cellarframe.pack(fill=tk.X)
        self.cellarframe["height"]=10

        self.bottomframe=tk.Frame(self.master)
        self.bottomframe.pack(side=tk.BOTTOM)
        self.bottomframe.pack(fill=tk.X, after=self.cellarframe)

        self.bottomMessageframe=tk.Frame(self.master)
        self.bottomMessageframe.pack(side=tk.BOTTOM)
        self.bottomMessageframe.pack(fill=tk.X, after=self.bottomframe)
        self.bottomMessageLabel=tk.Label(self.bottomMessageframe)
        self.bottomMessageLabel.pack(fill=tk.X)
                
        self.butBack=tk.Button(self.topframe, text="Back", command=self.closeWindow)
        self.butBack.pack(side=tk.RIGHT)
        
        self.buttonSave=ttk.Button(self.bottomframe, text="Save", command=self.insertUser)
        self.buttonSave.pack(side=tk.LEFT, padx=10)
         
        self.buttonCancel=ttk.Button(self.bottomframe, text="Cancel", command=self.closeWindow)
        self.buttonCancel.pack(side=tk.RIGHT, padx=10)

        self.dbconnection=dbconnect
        self.makeWindow()

    def makeWindow(self):
        self.Userframe = tk.Frame(self.master)
        self.Userframe.pack(fill=tk.X)
        self.UserE = ttk.Entry(self.Userframe)
        self.UserE['width']=30
        self.UserE.pack(side=tk.RIGHT)
        self.UserLabel=tk.Label(self.Userframe, text=" New User")
        self.UserLabel.pack(side=tk.LEFT)

    def closeWindow(self):
        self.master.destroy()
        
    def insertUser(self):
        self.bottomMessageLabel['fg']='green'
        try:
            cursor=self.dbconnection.cursor()
            NameUser=self.UserE.get()
    
            insert_query='insert into Users (NameUser) values("'+NameUser+'")'
            
            cursor.execute(insert_query)
            
            self.dbconnection.commit()
            self.bottomMessageLabel['text']='User was added successfully!!!'
        except Exception as e:
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='Something is wrong! Exception is ' +str(e)        