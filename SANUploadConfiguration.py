'''
Created on 23 May 2020

@author: idaymand
'''

import tkinter as tk
import tkinter.ttk as ttk
from tkinter import filedialog
from tkinter import simpledialog
import os, shutil
import tarfile
import paramiko
import tkinter
from datetime import datetime
import fpdf
from SANSettings import APPSettings

class SANUploadConfiguration(object):
    def __init__(self, master, dbconnect):
        self.master=master
        self.output_filename=''
        self.master.title("Upload Configuration")
        self.topframeBSD = tk.Frame(self.master)
        self.topframeBSD.pack(fill=tk.X)
        self.topBSDLabel=tk.Label(self.topframeBSD, text='בסד')
        self.topBSDLabel.pack(side=tk.RIGHT)
        self.topframe = tk.Frame(self.master)
        self.topframe.pack(fill=tk.X, after=self.topframeBSD)
                 
        self.cellarframe=tk.Frame(self.master)
        self.cellarframe.pack(side=tk.BOTTOM)
        self.cellarframe.pack(fill=tk.X)
        self.cellarframe["height"]=10
                

        self.bottomMessageframe=tk.Frame(self.master)
        self.bottomMessageframe.pack(side=tk.BOTTOM)
        self.bottomMessageframe.pack(fill=tk.X, after=self.cellarframe)
        self.bottomMessageLabel=tk.Label(self.bottomMessageframe, fg='green')
        self.bottomMessageLabel['height']=17
        self.bottomMessageLabel.pack(fill=tk.X)
        
        self.bottomframe=tk.Frame(self.master)
        self.bottomframe.pack(side=tk.BOTTOM)
        self.bottomframe.pack(fill=tk.X, after=self.bottomMessageframe)
        self.bottomframe['height']=10
 
        self.butBack=tk.Button(self.topframe, text="Back", command=self.closeWindow)
        self.butBack.pack(side=tk.RIGHT)
        
        self.buttonUpload=ttk.Button(self.bottomframe, text="Upload", command=self.uploadConfiguration)
        self.buttonUpload.pack(side=tk.RIGHT, padx=10)
         
        self.buttonSave=ttk.Button(self.bottomframe, text="Save", command=self.saveConfiguration)
        self.buttonSave.pack(side=tk.RIGHT, padx=10)

        self.buttonSave=ttk.Button(self.bottomframe, text="Clear", command=self.clearConfiguration)
        self.buttonSave.pack(side=tk.RIGHT, padx=10)

        self.buttonUpload=ttk.Button(self.bottomframe, text="Reset SerialNumber", command=self.resetSerialNumber)
        self.buttonUpload.pack(side=tk.LEFT, padx=10)

        self.dbconnection=dbconnect
        self.Settings=APPSettings(self.dbconnection)
        self.Settings.getSettings()
        self.setDefaultOutputFilename()
        if(self.default_output_filename!=""):
            self.makeWindow()

    def resetSerialNumber(self):
        self.bottomMessageLabel['fg']='green'
        self.bottomMessageLabel['text']=''
        initialVal = simpledialog.askinteger("Input", "What is new initial value?",
                                         parent=self.master,
                                         minvalue=1, maxvalue=10000)        
        try:
            cursor=self.dbconnection.cursor()
                        
            sqlQuery="delete from  Uploads"
            
            cursor.execute(sqlQuery)
            
            self.dbconnection.commit()
            sqlQuery="alter table Uploads AUTO_INCREMENT = "+str(initialVal)

            cursor.execute(sqlQuery)

            self.dbconnection.commit()
            self.bottomMessageLabel['text']='Initial value for serial number has reseted successfully!!!\r\n'
        except Exception as e:
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='Initial value reseting was fail!!! Exception is '+str(e)+'\r\n'
    
    def setDefaultOutputFilename(self):
        path_default_output_filename=os.path.dirname(self.Settings.Settings['TargzFilename'])
        if(path_default_output_filename == ""):
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='Default tar.gz file is not selected! Please select it on form Setting Folder'
            self.default_output_filename='';
            return
        if(path_default_output_filename != self.Settings.Settings['TargzDirectory']):
            dst_path=os.path.join(self.Settings.Settings['TargzDirectory'], os.path.basename(self.Settings.Settings['TargzFilename'])) 
            shutil.copy(self.Settings.Settings['TargzFilename'], dst_path)
            self.default_output_filename=dst_path
        else:
            self.default_output_filename=self.Settings.Settings['TargzFilename']
    
    def clearConfiguration(self):
        self.bottomMessageLabel['fg']='green'
        self.MachineIdE['text']=''
        self.EncryptLabel['text']=''
        self.bottomMessageLabel['text']=''
        self.output_filename=self.default_output_filename
        self.TargzLabelValue['text']=''
        self.DBOvenLabelValue['text']=''
        self.DBPCLabelValue['text']=''
        self.result_filename=''
        self.reportFileName=''
                           
    def makeWindow(self):
        self.FurnaceModelframe = tk.Frame(self.master, borderwidth = 1)
        self.FurnaceModelframe.pack(fill=tk.X)
        self.FurnaceModelCB = ttk.Combobox(self.FurnaceModelframe)
        self.FurnaceModelCB.pack(side=tk.RIGHT, padx=10)
        self.FurnaceModelCB['width']=30
        self.FurnaceModelLabel=tk.Label(self.FurnaceModelframe, text="  Model")
        self.FurnaceModelLabel.pack(side=tk.LEFT)
        self.FurnaceModelCB.bind("<<ComboboxSelected>>", self.onFurnaceModelSelect)

        self.Configurationframe = tk.Frame(self.master, borderwidth = 1)
        self.Configurationframe.pack(fill=tk.X)
        self.ConfigurationCB = ttk.Combobox(self.Configurationframe)
        self.ConfigurationCB.pack(side=tk.RIGHT, padx=10)
        self.ConfigurationCB['width']=30
        self.ConfigurationLabel=tk.Label(self.Configurationframe, text="  Configuration")
        self.ConfigurationLabel.pack(side=tk.LEFT)
        self.ConfigurationCB.bind("<<ComboboxSelected>>", self.onConfigurationSelect)

        self.Userframe = tk.Frame(self.master, borderwidth = 1)
        self.Userframe.pack(fill=tk.X)
        self.UserCB = ttk.Combobox(self.Userframe, state='readonly')
        self.UserCB.pack(side=tk.RIGHT, padx=10)
        self.UserCB['width']=30
        self.UserLabel=tk.Label(self.Userframe, text="  User")
        self.UserLabel.pack(side=tk.LEFT)
        self.UserCB.bind("<<ComboboxSelected>>", self.onUserSelect)
        self.UserCB['values'], self.idUserList, startvalue = self.combo_input("select  NameUser,  IDUser from Users")
        if(startvalue != ''):
            self.UserCB.set(startvalue)
            self.onUserSelect(0)

        self.IPAddressframe = tk.Frame(self.master, borderwidth = 1)
        self.IPAddressframe.pack(fill=tk.X)
        self.IPAddressE = ttk.Entry(self.IPAddressframe)
        self.IPAddressE['width']=30
        self.IPAddressE.insert(0, '10.0.0.185')
        self.IPAddressE.pack(side=tk.RIGHT, padx=10)
        self.buttonTest=ttk.Button(self.IPAddressframe, text="Test", command=self.testIPAddress)
        self.buttonTest.pack(side=tk.RIGHT, after=self.IPAddressE)
        self.IPAddressLabel=tk.Label(self.IPAddressframe, text=" IP Address")
        self.IPAddressLabel.pack(side=tk.LEFT)
        
        self.MachineIdframe = tk.Frame(self.master, borderwidth = 1)
        self.MachineIdframe.pack(fill=tk.X)
        self.MachineIdE = ttk.Label(self.MachineIdframe)
        self.MachineIdE['width']=30
        self.MachineIdE.pack(side=tk.RIGHT, padx=10)
        self.MachineIdLabel=tk.Label(self.MachineIdframe, text=" MachineId")
        self.MachineIdLabel.pack(side=tk.LEFT)
        
        self.Encryptframe = tk.Frame(self.master, borderwidth = 1)
        self.Encryptframe.pack(fill=tk.X)
        self.EncryptLabel = ttk.Label(self.Encryptframe)
        self.EncryptLabel['width']=30
        self.EncryptLabel.pack(side=tk.RIGHT, padx=10)
        self.butEncrypt=tk.Button(self.Encryptframe, text=" Encrypt ", command=self.makeEncrypt)
        self.butEncrypt.pack(side=tk.LEFT)
        
        self.Serialframe = tk.Frame(self.master, borderwidth = 1)
        self.Serialframe.pack(fill=tk.X)
        self.SerialEntryValue = ttk.Label(self.Serialframe)
        self.SerialEntryValue['width']=40
        self.SerialEntryValue.pack(side=tk.RIGHT, padx=10)
        self.SerialButton=tk.Button(self.Serialframe, text=" Serial", command=self.makeSerialNumberConfiguration)
        self.SerialButton.pack(side=tk.LEFT)
        

        self.Targzframe = tk.Frame(self.master, borderwidth = 1)
        self.Targzframe.pack(fill=tk.X)
        self.TargzLabelValue = ttk.Label(self.Targzframe)
        self.TargzLabelValue['width']=30
        self.TargzLabelValue.pack(side=tk.RIGHT, padx=10)
        self.TargzButton=tk.Button(self.Targzframe, text=" Tar.gz select", command=self.selectOutputFileName)
        self.TargzButton.pack(side=tk.LEFT)

        self.DBOvenframe = tk.Frame(self.master, borderwidth = 1)
        self.DBOvenframe.pack(fill=tk.X)
        self.DBOvenLabelValue = ttk.Label(self.DBOvenframe)
        self.DBOvenLabelValue['width']=30
        self.DBOvenLabelValue.pack(side=tk.RIGHT, padx=10)
        self.buttonDownloadDatabaseFromRPI=ttk.Button(self.DBOvenframe, text="DB Oven", command=self.downloadDatabaseFromRPI)
        self.buttonDownloadDatabaseFromRPI.pack(side=tk.LEFT)

        self.DBPCframe = tk.Frame(self.master, borderwidth = 1)
        self.DBPCframe.pack(fill=tk.X)
        self.DBPCLabelValue = ttk.Label(self.DBPCframe)
        self.DBPCLabelValue['width']=30
        self.DBPCLabelValue.pack(side=tk.RIGHT, padx=10)
        self.buttonUploadDatabaseToRPI=ttk.Button(self.DBPCframe, text="DB PC", command=self.uploadDatabaseToRPI)
        self.buttonUploadDatabaseToRPI.pack(side=tk.LEFT)

        self.FurnaceModelCB['values'], self.idFurnaceModelList, startvalue = self.combo_input("select  FurnaceModelName,  IDFurnaceModel from FurnaceModels")
        if(startvalue != ''):
            self.FurnaceModelCB.set(startvalue)
            self.onFurnaceModelSelect(0)

    def onFurnaceModelSelect(self, event):
        self.clearConfiguration()
        self.SerialNumber=''
        self.SerialEntryValue['text']=''
        indexSelect=self.FurnaceModelCB.current()
        self.idFurnaceModel=self.idFurnaceModelList[indexSelect];
        self.ConfigurationCB['values'], self.idConfigurationList, startvalue = self.combo_input("select NameConfiguration,  IDConfiguration from Configuration where IDFurnaceModel="+str(self.idFurnaceModel))
        self.ConfigurationCB.set(startvalue)
        self.onConfigurationSelect(0)
        self.makeSerialNumberConfiguration();
    
    def onConfigurationSelect(self, event):
        indexSelect=self.ConfigurationCB.current()
        if(indexSelect>=0):
            self.idConfiguration=self.idConfigurationList[indexSelect];
        else:
            self.idConfiguration=0;
    
    def onUserSelect(self, event):
        indexSelect=self.UserCB.current()
        if(indexSelect>=0):
            self.idUser=self.idUserList[indexSelect];
        else:
            self.idUser=0;

    def closeWindow(self):
        self.master.destroy()
    
    def createReport(self):
        self.bottomMessageLabel['fg']='green'
        if(self.Settings.Settings['ReportDirectory']==''):
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='Report directory is not selected!!! Please select on form Settings.'
            return
        if(self.Settings.Settings['PictureDirectory']==''):
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='Picture directory is not selected!!! Please select on form Settings.'
            return
        
        pdf_obj=fpdf.FPDF()
        pdf_obj.add_page()
        pdf_obj.set_xy(0, 0)
        pdf_obj.set_font('arial', 'BU', 18.0)
        pdf_obj.multi_cell(w=0, h=40.0, align='C', txt="Serial number "+self.SerialNumber, border=0)
        pdf_obj.set_font('arial', 'I', 11.0)
        pdf_obj.multi_cell(w=0, h=5.0, align='L', txt="Date: "+datetime.now().strftime('%d-%m-%y %h:%m'), border=0)
        pdf_obj.multi_cell(w=0, h=5.0, align='L', txt="Model: "+self.FurnaceModelCB.get(), border=0)
        pdf_obj.multi_cell(w=0, h=5.0, align='L', txt="License: "+str(self.EncryptLabel['text']), border=0)
        pdf_obj.multi_cell(w=0, h=5.0, align='L', txt="Tar.gz: "+os.path.basename(self.output_filename), border=0)
        pdf_obj.multi_cell(w=0, h=5.0, align='L', txt="User: "+self.UserCB.get(), border=0)
        if(self.DBOvenLabelValue['text']!=''):
            pdf_obj.multi_cell(w=0, h=5.0, align='L', txt="Database file: "+self.DBOvenLabelValue['text'], border=0)
        if(self.DBPCLabelValue['text']!=''):
            pdf_obj.multi_cell(w=0, h=5.0, align='L', txt="Database file: "+self.DBPCLabelValue['text'], border=0)
        curY=pdf_obj.get_y()
        pdf_obj.line(0, curY+10, 100, curY+10)
        self.pictureFileName=self.FurnaceModelCB.get()+'.jpg'
        self.pictureFileName=os.path.join(self.Settings.Settings['PictureDirectory'], self.pictureFileName)
        if(os.path.exists(self.pictureFileName)):
            curX=0
            curY=curY+10
            pdf_obj.image(self.pictureFileName, curX, curY, 0, 300)
        else:
            self.pictureFileName='default.jpg'
            self.pictureFileName=os.path.join(self.Settings.Settings['PictureDirectory'], self.pictureFileName)
            if(os.path.exists(self.pictureFileName)):
                curX=0
                curY=curY+10
                pdf_obj.image(self.pictureFileName, curX, curY, 0, 300)
            else:
                self.pictureFileName=''
                
        self.reportFileName=self.SerialNumber+'.pdf';
        self.reportFileName=os.path.join(self.Settings.Settings['ReportDirectory'], self.reportFileName)
        pdf_obj.output(self.reportFileName, 'F')
        self.bottomMessageLabel['text']=self.bottomMessageLabel['text']+ 'Saving file '+os.path.basename(self.reportFileName)+'\r\n'
        
        
    def write_file(self, data, filename):
        # Convert binary data to proper format and write it on Hard Disk
        with open(filename, 'wb') as file:
            if (isinstance(data, str)):
                data=data.encode()
            file.write(data)
            file.close()
    
    def downloadDataItem(self, select_query, new_filename):
        cursor=self.dbconnection.cursor()
        cursor.execute(select_query)
        for row in cursor.fetchall():
            filename=row[0]
            filedata=row[1]
            name, ext=os.path.splitext(filename)
            filename=new_filename+ext
            filename=os.path.join(self.downloadDirectory, filename)
            self.write_file(filedata, filename)
        
    def selectOutputFileName(self):
        self.bottomMessageLabel['text']=''
        self.DBOvenLabelValue['text']=''
        self.DBPCLabelValue['text']=''
        self.output_filename=filedialog.askopenfilename(parent=self.master, initialdir=self.Settings.Settings['TargzDirectory'], title='Please select tar.gz file for update')
        if(self.output_filename==''):
            self.output_filename=self.default_output_filename
        self.TargzLabelValue['text']=os.path.basename(self.output_filename) 
    
    def downloadDataConfiguration(self):        
            
        index_select=self.ConfigurationCB.current()
        self.idConfiguration=self.idConfigurationList[index_select]
        
        select_query="select NameImage, ImageData from Images where IDImage in (select IDStartupImage from Configuration where IDConfiguration = "+str(self.idConfiguration)+')'
        self.downloadDataItem(select_query, 'LogoStart')
        
        select_query="select NameImage, ImageData from Images where IDImage in (select IDClientLogoImage from Configuration where IDConfiguration = "+str(self.idConfiguration)+')'
        self.downloadDataItem(select_query, 'LogoApp')
        
        select_query="select NameImage, ImageData from Images where IDImage in (select IDManufactureLogoImage from Configuration where IDConfiguration = "+str(self.idConfiguration)+')'
        self.downloadDataItem(select_query, 'MatLogo')
        
        select_query="select NameVIAProgram, VIAProgramData from VIAPrograms where IDVIAProgram in (select IDVIAProgram from Configuration where IDConfiguration = "+str(self.idConfiguration)+')'
        self.downloadDataItem(select_query, 'Csv_export')

        select_query="select NameManual, ManualData from Manuals where IDManual in (select IDManual from Configuration where IDConfiguration = "+str(self.idConfiguration)+')'
        self.downloadDataItem(select_query, 'UserManual')

        
    def downloadDatabaseFromRPI(self):
        self.bottomMessageLabel['fg']='green'
        self.result_filename=''
        self.DBPCLabelValue['text']=''
        self.bottomMessageLabel['text']=''
        if(self.output_filename==''):
            self.output_filename=self.default_output_filename
        self.bottomMessageLabel['text']=''
        sftp_client, ssh_client=self.getSftp()
        src_path='/mnt/database/database.sqlite'
        
        self.destination_filename=tkinter.filedialog.asksaveasfilename(parent=self.master, initialdir=self.Settings.Settings['DBDirectory'], title='Save database.sqlite from RPI as')
        if(self.destination_filename==''):
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='You did not select file for save!!! Operation is canceled!!!'
            return
        self.master.update()
        filename, fileext=os.path.splitext(self.destination_filename)
        if(fileext==''):
            self.destination_filename=self.destination_filename+'.sqlite'
        self.DBOvenLabelValue['text']=os.path.basename(self.destination_filename)
        self.downloadDirectory=os.path.dirname(self.destination_filename)
        sftp_client.get(src_path, self.destination_filename)
        dst_path=os.path.join(os.path.dirname(self.destination_filename), 'database.sqlite')
        sftp_client.get(src_path, dst_path)
        sftp_client.close()
        ssh_client.close()
        self.bottomMessageLabel['text']='Database dowloaded success!!!\r\n'
        self.master.update()
        result_filename=os.path.splitext(os.path.splitext(os.path.basename(self.output_filename))[0])[0]+datetime.now().strftime('-%m-%y')+'.tar.gz'
        self.updateTargz('database.sqlite', result_filename)
        self.bottomMessageLabel['text']=self.bottomMessageLabel['text']+'Name of result file is ' + result_filename+'\r\n'
        self.output_filename=os.path.join(os.path.dirname(self.output_filename), result_filename)
        self.TargzLabelValue['text']=os.path.basename(self.output_filename)
        self.master.update()
        
    def updateTargz(self, filename='', result_filename=''):    
        self.bottomMessageLabel['fg']='green'
        self.bottomMessageLabel['text']=self.bottomMessageLabel['text']+ 'Extract all data from file '+os.path.basename(self.output_filename)+', please wait\r\n'
        self.master.update()
        upd_tar=tarfile.open(self.output_filename) 
        upd_tar.extractall(os.path.dirname(self.output_filename))
        upd_tar.close()
        bin_path=os.path.join(os.path.dirname(self.output_filename), 'bin')
        if(filename==''):
            files_in_directory=os.listdir(self.downloadDirectory)
            for file in files_in_directory:
                shutil.move(os.path.join(self.downloadDirectory,file), os.path.join(bin_path,file))
        else:
            shutil.move(os.path.join(self.downloadDirectory,filename), os.path.join(bin_path,filename))
        curpath=os.curdir
        os.chdir(os.path.dirname(self.output_filename))
        if(result_filename==''):    
            self.result_filename=os.path.join(os.path.dirname(self.output_filename), os.path.splitext(os.path.splitext(os.path.basename(self.output_filename))[0])[0]+'-'+self.SerialNumber+'.tar.gz')
        else:
            self.result_filename=os.path.join(os.path.dirname(self.output_filename), result_filename) 
        configuration_tar=tarfile.open(self.result_filename, "w:gz")
        files_in_bin_directory=os.listdir(bin_path)
        for file in files_in_bin_directory:
            if(file == 'database.sqlite'):
                self.bottomMessageLabel['text']=self.bottomMessageLabel['text']+ ' database.sqlite is inside of file '+os.path.basename(self.result_filename)+'\r\n'
                break
        self.bottomMessageLabel['text']=self.bottomMessageLabel['text']+ 'Create file '+os.path.basename(self.result_filename)+', adding all data to new archive, please wait\r\n'
        self.master.update()
        self.master.focus()
        configuration_tar.add('bin')
        if(os.path.exists(os.path.join(os.path.dirname(self.output_filename), 'webapp'))):
            configuration_tar.add('webapp')
        configuration_tar.close()
        shutil.rmtree(os.path.join(os.path.dirname(self.output_filename), 'bin'))
        if(os.path.exists(os.path.join(os.path.dirname(self.output_filename), 'webapp'))):
            shutil.rmtree(os.path.join(os.path.dirname(self.output_filename), 'webapp'))
        os.chdir(curpath)
        self.bottomMessageLabel['text']=self.bottomMessageLabel['text']+ os.path.basename(self.result_filename)+' created successfully\r\n'
        self.master.update()
            
    def uploadDatabaseToRPI(self):
        self.bottomMessageLabel['fg']='green'
        self.result_filename=''
        self.bottomMessageLabel['text']=''
        self.DBOvenLabelValue['text']=''
        if(self.output_filename==''):
            self.output_filename=self.default_output_filename
        upload_filename=tkinter.filedialog.askopenfilename(parent=self.master, initialdir=self.Settings.Settings['DBDirectory'], title='Select database file for upload to RPI')
        if(upload_filename == ''):
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='You did not select file for upload!!! Operation canceled!!!'
            return
        self.DBPCLabelValue['text']=os.path.basename(upload_filename)
        self.master.update()
        self.downloadDirectory=os.path.dirname(upload_filename)
        result_filename=os.path.splitext(os.path.splitext(os.path.basename(self.output_filename))[0])[0]+datetime.now().strftime('-%m-%y')+'.tar.gz'
        copy_upload_filename=os.path.join(os.path.dirname(upload_filename), 'database.sqlite')
        shutil.copy(upload_filename, copy_upload_filename)
        self.updateTargz('database.sqlite', result_filename)
        self.bottomMessageLabel['text']=self.bottomMessageLabel['text']+'Name of result file is ' + result_filename+'\r\n'
        self.output_filename=os.path.join(os.path.dirname(self.output_filename), result_filename)
        self.TargzLabelValue['text']=os.path.basename(self.output_filename)
        self.master.update()
            
    def saveConfiguration(self, isIncrease=False):
        self.bottomMessageLabel['fg']='green'
        self.bottomMessageLabel['text']=''
        TCP_IP = self.IPAddressE.get()
        if(TCP_IP=='' and isIncrease==True):
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']="Please input IP address of furnace!!!"
            return 0
        if(self.FurnaceModelCB.get()==''):
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']="Please select model of furnace!!!"
            return 0
        if(self.output_filename==''):
            self.output_filename=self.default_output_filename
            self.TargzLabelValue['text']=os.path.basename(self.output_filename)
        
        self.downloadDirectory=os.path.join(os.path.abspath(os.path.curdir), 'temp')
        if(os.path.exists(self.downloadDirectory) == False):
            os.makedirs(self.downloadDirectory, exist_ok=True)

        if(isIncrease==False):
            self.result_filename=filedialog.asksaveasfilename(initialdir=self.Settings.Settings['TargzDirectory'], title='Save new tar.gz file as')
            if(self.result_filename==''):
                return 0
            result_filename_ext=os.path.splitext(os.path.basename(self.result_filename))[1]    
            if(result_filename_ext !='.gz'):
                self.result_filename=self.result_filename+'.tar.gz'
        
        if(isIncrease==True):            
            self.getMachineId()
            self.master.focus()
            self.makeEncrypt()


        if(self.ConfigurationCB.get()!=''):
            self.downloadDataConfiguration()
            self.bottomMessageLabel['text']=self.bottomMessageLabel['text']+ 'Choosed configuration was downloaded successfully\r\n'
            self.master.update()
            
        if(isIncrease == True):
            self.SerialNumber=self.SerialEntryValue['text'];
        
        if(isIncrease == True):# and self.SerialEntryValue.get()==''):
            self.makeSerialNumberConfiguration()
        
        if(isIncrease == True):
            self.makeConfigFile()
            self.makeLicense()
            self.bottomMessageLabel['text']=self.bottomMessageLabel['text']+ 'Config.txt, License.dat were created successfully\r\n'
            self.master.update()
        
        if(isIncrease==False and self.result_filename==''):
            self.result_filename=os.path.splitext(os.path.splitext(os.path.basename(self.output_filename))[0])[0]+datetime.now().strftime('-%m-%y')+'.tar.gz'
        self.updateTargz('', self.result_filename)
        shutil.rmtree(self.downloadDirectory)
        if(isIncrease==False):
            self.bottomMessageLabel['text']=self.bottomMessageLabel['text']+'Name of result file is ' + self.result_filename+'\r\n'
            self.output_filename=os.path.join(os.path.dirname(self.output_filename), self.result_filename)
            self.TargzLabelValue['text']=os.path.basename(self.output_filename)
            self.master.update()
        return 1
    
    def uploadConfiguration(self):
        self.bottomMessageLabel['fg']='green'
        self.result_filename=''
        ret=self.saveConfiguration(True)
        if(ret == 0 ):
            return
        if(self.Settings.Settings['UploadTargzDirectory']!=''):
            dst_upload=os.path.join(self.Settings.Settings['UploadTargzDirectory'], os.path.basename(self.result_filename))
            shutil.move(self.result_filename, dst_upload)
            self.result_filename=dst_upload
            self.bottomMessageLabel['text']=self.bottomMessageLabel['text']+ ' ' +os.path.basename(self.result_filename)+' was moved to folder for upload'+'\r\n'
            self.master.update()
        self.uploadTarGz()
        self.bottomMessageLabel['text']=self.bottomMessageLabel['text']+ ' '+ os.path.basename(self.result_filename)+' was uploaded success\r\n'
        self.master.update()
        self.updateInfoAboutUpload()
        self.createReport()
        self.master.update()
        self.master.focus()
        self.bottomMessageLabel['text']=self.bottomMessageLabel['text']+ 'Finish of operation upload\r\n'
        self.master.update()
        
    
    def updateInfoAboutUpload(self):
        try:
            cursor=self.dbconnection.cursor()
            
            query='update Uploads set SerialNumber="'+self.SerialNumber+'", IDUser='+str(self.idUser)+' where IDUpload='+str(self.IDUpload)
            cursor.execute(query)
        except Exception as e:
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']=self.bottomMessageLabel['text']+'update upload information is wrong, '+str(e)+'\r\n'
        finally:
            cursor.close()
            
    def deleteSerialNumberConfiguration(self):
        cursor= self.dbconnection.cursor()
        
        query='delete from Uploads where  IDUpload= select max(IDUpload) from Uploads where  IDFurnaceModel='+str(self.idFurnaceModel)+' and IDConfiguration=' + str(self.idConfiguration)
        try:
            cursor.execute(query)
        except Exception as e:
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']=self.bottomMessageLabel['text']+' Exception is ' + str(e)
        finally:
            cursor.close()
            
    def makeSerialNumberConfiguration(self):
        self.SerialEntryValue['text']='';
        
        cursor= self.dbconnection.cursor()
        
        query='insert into Uploads(IDFurnaceModel, IDConfiguration, DateUpload) values ('+str(self.idFurnaceModel)+', '+str(self.idConfiguration)+', now())'
        try:
            cursor.execute(query)
            self.dbconnection.commit()
        except Exception as e:
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']=self.bottomMessageLabel['text']+' Exception is ' + str(e)
            cursor.close()

        query='select IDUpload as inumber, concat(cast(extract(day from DateUpload) as char(2)), cast(extract(month from DateUpload) as char(2)), CAST(extract(year from DateUpload) as char(2))) as dnumber from Uploads  where IDUpload=(select max(IDUpload) from Uploads where IDFurnaceModel='+str(self.idFurnaceModel)+' and IDConfiguration=' + str(self.idConfiguration)+')'
        
        try:
            cursor.execute(query)
            for row in cursor.fetchall():
                self.SerialNumber=self.FurnaceModelCB.get().replace(" ", "")+'-'+str(row[1])+'-'+str(row[0])
                self.Model=self.FurnaceModelCB.get().replace(" ", "")
                self.IDUpload=row[0]
            cursor.close()
            self.SerialEntryValue['text']= self.SerialNumber;
            self.master.update()
        except Exception as e:
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']=self.bottomMessageLabel['text']+' Exception is ' + str(e)
            cursor.close()

    def encrypt(self, in_str):
        key='abcdefghabcdefgh'
        encrypt=B''
        for index1 in range(0, len(key)):
            val_encrypt=ord(in_str[index1])+ord(key[index1])
            encrypt=encrypt+bytes([val_encrypt])
        return encrypt.hex()
            
    def decrypt(self, in_encrypt):
        key='abcdefghabcdefgh'
        str_out=''
        for index1 in range(0, len(key)):
            val_decrypt=in_encrypt[index1] - ord(key[index1])
            str_out=str_out+chr(val_decrypt)
        return str_out

    def makeEncrypt(self):
        if(self.MachineIdE['text']==''):
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='Press please Test button for getting Mac address!!!'
            return
        data=self.MachineIdE['text']
        data=data.replace(" ", "")
        data=data.replace("\n", "")
        encode=self.encrypt(data)
        self.EncryptLabel['text']=encode
#        str_out=self.decrypt(encode)
        self.master.update()

    
    def makeLicense(self):
        self.filename_license="license.dat"
        self.filename_license=os.path.join(self.downloadDirectory, self.filename_license)
        with open(self.filename_license, 'w') as file:
            if(self.EncryptLabel['text'] == ''):
                self.makeEncrypt()
            data=self.EncryptLabel['text']
            file.write(data)
            file.close()
        
    def makeConfigFile(self):
        self.filename_config="config.txt"
        self.filename_config=os.path.join(self.downloadDirectory, self.filename_config)
        
        with open(self.filename_config, 'wb') as file:
            data='Serial='+self.SerialNumber+'\r\n'
            file.write(data.encode())
            data='Model='+self.Model+'\r\n'
            file.write(data.encode())
            file.close()
                
    def testIPAddress(self):
        self.bottomMessageLabel['fg']='green'
        self.bottomMessageLabel['text']=''
        TCP_IP = self.IPAddressE.get()
        if(TCP_IP==''):
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']="Please input IP address of furnace!!!"
            return
        try:
            self.getMachineId()
            self.bottomMessageLabel['text']='Connection success!!!'
        except Exception as e:
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']=self.bottomMessageLabel['text']+' Exception is ' + str(e)
    
    def getMachineId(self):
        self.MachineIdE['text']=''
        sftp_client, ssh_client=self.getSftp()
        remote_file=sftp_client.open('/proc/cpuinfo')
        try:
            for line in remote_file:#
                line=line.replace("\t", "")
                index_MachineId=line.find('Serial:')
                if (index_MachineId>=0):
                    index_MachineId=index_MachineId+len('Serial:')
                    MachineId=line[index_MachineId:]
                    self.MachineIdE['text']= MachineId
                    break
        except Exception as e:
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']=self.bottomMessageLabel['text']+' Exception is ' + str(e)
        finally:
            remote_file.close()
            sftp_client.close()
            ssh_client.close()
        
        #=======================================================================
        # cmd="arp -a "+remotehost
        # p=subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        # output, errors = p.communicate()
        # if output is not None :
        #     output = output.decode('ascii')
        #     if sys.platform in ['linux','linux2']:
        #         for i in output.split("\n"):
        #             if remotehost in i:
        #                 for j in i.split():
        #                     if ":" in j:
        #                         self.SerialE.insert(0, "%s" % (j))
        #     elif sys.platform in ['darwin']:
        #         for i in output.split("\n"):
        #             if remotehost+')' in i:
        #                 for j in i.split():
        #                     if ":" in j:
        #                         self.SerialE.insert(0, "%s" % (j))
        #     elif sys.platform in ['win32']:
        #         item =  output.split("\n")[-2]
        #         if remotehost in item:
        #             self.SerialE.insert(0, "%s" %(item.split()[1]))
        #=======================================================================
        self.bottomMessageLabel['text']='Mac address got success\r\n'
    
    def getSftp(self):
        target = self.IPAddressE.get()
        un = 'pi'
        passwd = 'raspberry'
        ssh_client=paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh_client.connect(hostname=target,  username=un, password=passwd)
        sftp=ssh_client.open_sftp()
        return sftp, ssh_client
    
    def uploadTarGz(self):
        dst_filename=os.path.join('/mnt/storage/laravel/storage/uploads/', os.path.basename(self.result_filename))
        try:
            sftp, ssh_client=self.getSftp()
            sftp.put(self.result_filename, dst_filename)
            sftp.close()
            stdin, stdout, stderr=ssh_client.exec_command('cd /mnt/app/bin\n sudo chmod 777 upgrade1.sh\n sudo chmod 777 shenpaz-exe\n cd /mnt/storage/laravel/storage/uploads/\n sudo rm last\n sudo ln -s /mnt/storage/laravel/storage/uploads/'+os.path.basename(self.result_filename)+' last \n cd /mnt/app/bin\n sudo ./upgrade1.sh\n')
            stderr.read(1024)
        except Exception as e:
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']=self.bottomMessageLabel['text']+str(e)
        finally:    
            ssh_client.close()
                              
    def combo_input(self, query):
        cursor= self.dbconnection.cursor()
        
        cursor.execute(query)
    
        values = []
        keys = []
        
        first=True
        startvalue=''
        for row in cursor.fetchall():
            if(first == True):
                startvalue=row[0]
                first=False
            values.append(row[0])
            keys.append(row[1])
    
        return values, keys, startvalue  
