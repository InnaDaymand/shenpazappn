/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `configuration` (
  `IDConfiguration` int NOT NULL AUTO_INCREMENT,
  `NameConfiguration` varchar(100) DEFAULT NULL,
  `IDFurnaceModel` int NOT NULL,
  `IDStartupImage` int DEFAULT NULL,
  `IDManual` int DEFAULT NULL,
  `IDVIAProgram` int DEFAULT NULL,
  `IDClientLogoImage` int DEFAULT NULL,
  `IDManufactureLogoImage` int DEFAULT NULL,
  PRIMARY KEY (`IDConfiguration`),
  KEY `FurnaceModels_Configuration` (`IDFurnaceModel`),
  KEY `Images_Configuration` (`IDStartupImage`),
  KEY `Manuals_Configuration` (`IDManual`),
  KEY `VIAPrograms_Configuration` (`IDVIAProgram`),
  CONSTRAINT `configuration_ibfk_1` FOREIGN KEY (`IDFurnaceModel`) REFERENCES `furnacemodels` (`IDFurnaceModel`),
  CONSTRAINT `configuration_ibfk_2` FOREIGN KEY (`IDStartupImage`) REFERENCES `images` (`IDImage`),
  CONSTRAINT `configuration_ibfk_4` FOREIGN KEY (`IDManual`) REFERENCES `manuals` (`IDManual`),
  CONSTRAINT `configuration_ibfk_5` FOREIGN KEY (`IDVIAProgram`) REFERENCES `viaprograms` (`IDVIAProgram`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `furnacemodels` (
  `IDFurnaceModel` int NOT NULL AUTO_INCREMENT,
  `FurnaceModelName` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`IDFurnaceModel`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `images` (
  `IDImage` int NOT NULL AUTO_INCREMENT,
  `IDTypeImage` int NOT NULL,
  `ImageData` longblob,
  `NameImage` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`IDImage`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `manuals` (
  `IDManual` int NOT NULL AUTO_INCREMENT,
  `ManualData` longblob,
  `NameManual` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`IDManual`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `settings` (
  `IdSetting` int NOT NULL AUTO_INCREMENT,
  `NameSetting` varchar(40) DEFAULT NULL,
  `ValueSetting` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`IdSetting`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `typeimages` (
  `IDTypeImage` int NOT NULL AUTO_INCREMENT,
  `NameTypeImage` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`IDTypeImage`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `uploads` (
  `IDUpload` int NOT NULL AUTO_INCREMENT,
  `IDFurnaceModel` int DEFAULT NULL,
  `IDConfiguration` int DEFAULT NULL,
  `DateUpload` datetime DEFAULT NULL,
  `SerialNumber` varchar(50) DEFAULT NULL,
  `IDUser` int DEFAULT NULL,
  PRIMARY KEY (`IDUpload`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `IDUser` int NOT NULL AUTO_INCREMENT,
  `NameUser` varchar(100) NOT NULL,
  PRIMARY KEY (`IDUser`),
  UNIQUE KEY `UniqueNameUser` (`NameUser`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `viaprograms` (
  `IDVIAProgram` int NOT NULL AUTO_INCREMENT,
  `VIAProgramData` longblob,
  `NameVIAProgram` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`IDVIAProgram`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
delimiter //
CREATE PROCEDURE `sUpdateSettings`(NameS varchar(40), ValS varchar(255))
begin
declare ids int;
select IDSetting into ids from Settings where NameSetting like NameS;
if ifnull(ids,1)=1 or ids=0 then insert into Settings(NameSetting, ValueSetting) values(NameS, ValS);
else update Settings set ValueSetting=ValS where IDSetting=ids;
end if;
end //
delimiter ;