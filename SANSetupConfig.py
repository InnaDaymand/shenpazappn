'''
Created on 18 May 2020

@author: idaymand
'''
import tkinter as tk
import tkinter.ttk as ttk

class SANSetupConfig(object):
    '''
    classdocs    '''


    def __init__(self, master, dbconnect):
        self.master=master
        self.master.title("Setup Configuration")
        self.topframeBSD = tk.Frame(self.master)
        self.topframeBSD.pack(fill=tk.X)
        self.topBSDLabel=tk.Label(self.topframeBSD, text='בסד')
        self.topBSDLabel.pack(side=tk.RIGHT)
        self.topframe = tk.Frame(self.master)
        self.topframe.pack(fill=tk.X, after=self.topframeBSD)
                 
                 
        self.cellarframe=tk.Frame(self.master)
        self.cellarframe.pack(side=tk.BOTTOM)
        self.cellarframe.pack(fill=tk.X)
        self.cellarframe["height"]=10

        self.bottomframe=tk.Frame(self.master)
        self.bottomframe.pack(side=tk.BOTTOM)
        self.bottomframe.pack(fill=tk.X, after=self.cellarframe)
                
        self.bottomMessageframe=tk.Frame(self.master)
        self.bottomMessageframe.pack(side=tk.BOTTOM)
        self.bottomMessageframe.pack(fill=tk.X, after=self.bottomframe)
        self.bottomMessageLabel=tk.Label(self.bottomMessageframe)
        self.bottomMessageLabel.pack(fill=tk.X)

        self.butBack=tk.Button(self.topframe, text="Back", command=self.closeWindow)
        self.butBack.pack(side=tk.RIGHT)
        
        self.IsUpdate=tk.IntVar()
        self.IsInsert=tk.IntVar()
        self.buttonUpdate=ttk.Button(self.bottomframe, text="Override Pre-made config", command=self.updateConfiguration)
        self.buttonUpdate.pack(side=tk.LEFT, padx=10)
        self.buttonUpdate.var=self.IsUpdate
        self.buttonUpdate['state']='disable'
    

        self.dbconnection=dbconnect
        self.makeWindow()
        
    
    def makeWindow(self):
        self.FurnaceModelframe = tk.Frame(self.master)
        self.FurnaceModelframe.pack(fill=tk.X)
        self.FurnaceModelCB = ttk.Combobox(self.FurnaceModelframe)
        self.FurnaceModelCB['width']=30
        self.FurnaceModelCB.pack(side=tk.RIGHT)
        self.FurnaceModelLabel=tk.Label(self.FurnaceModelframe, text="  Model Type")
        self.FurnaceModelLabel.pack(side=tk.LEFT)
        self.FurnaceModelCB.bind("<<ComboboxSelected>>", self.onFurnaceModelSelect)
        
        self.Configurationframe = tk.Frame(self.master)
        self.Configurationframe.pack(fill=tk.X)
        self.ConfigurationCB = ttk.Combobox(self.Configurationframe, validate='key', validatecommand=self.onInsert)
        self.ConfigurationCB.pack(side=tk.RIGHT)
        self.ConfigurationCB['width']=30
        self.ConfigurationCB.bind("<<ComboboxSelected>>", self.onConfigurationSelect)
        self.ConfigurationLabel=tk.Label(self.Configurationframe, text="  Name of Package Configuration")
        self.ConfigurationLabel.pack(side=tk.LEFT)
 
        self.ConfigurationButtonsframe = tk.Frame(self.master)
        self.ConfigurationButtonsframe.pack(fill=tk.X)
        self.buttonInsert=ttk.Button(self.ConfigurationButtonsframe, text="SAVE", command=self.insertConfiguration)
        self.buttonInsert.pack(side=tk.LEFT, padx=10)
        self.buttonInsert.var=self.IsInsert
        self.buttonInsert['state']='disable'
       
        self.buttonDelete=ttk.Button(self.ConfigurationButtonsframe, text="DELETE", command=self.deleteConfiguration)
        self.buttonDelete.pack(side=tk.LEFT, padx=10)

        self.StartupImgframe = tk.Frame(self.master)
        self.StartupImgframe.pack(fill=tk.X)
        self.StartupImgCB = ttk.Combobox(self.StartupImgframe, validate='key', validatecommand=self.onUpdate)
        self.StartupImgCB.pack(side=tk.RIGHT)
        self.StartupImgCB['width']=30
        self.StartupImgCB.bind("<<ComboboxSelected>>", self.onUpdate)
        self.StartupImgLabel=tk.Label(self.StartupImgframe, text="  Startup Logo")
        self.StartupImgLabel.pack(side=tk.LEFT)

        self.ClientLogoImgframe = tk.Frame(self.master)
        self.ClientLogoImgframe.pack(fill=tk.X)
        self.ClientLogoImgCB = ttk.Combobox(self.ClientLogoImgframe, validate='key', validatecommand=self.onUpdate)
        self.ClientLogoImgCB.pack(side=tk.RIGHT)
        self.ClientLogoImgCB['width']=30
        self.ClientLogoImgCB.bind("<<ComboboxSelected>>", self.onUpdate)
        self.ClientLogoImgLabel=tk.Label(self.ClientLogoImgframe, text="  Application Logo")
        self.ClientLogoImgLabel.pack(side=tk.LEFT)

        self.ManufactureLogoImgframe = tk.Frame(self.master)
        self.ManufactureLogoImgframe.pack(fill=tk.X)
        self.ManufactureLogoImgCB = ttk.Combobox(self.ManufactureLogoImgframe, validate='key', validatecommand=self.onUpdate)
        self.ManufactureLogoImgCB.pack(side=tk.RIGHT)
        self.ManufactureLogoImgCB['width']=30
        self.ManufactureLogoImgCB.bind("<<ComboboxSelected>>", self.onUpdate)
        self.ManufactureLogoImgLabel=tk.Label(self.ManufactureLogoImgframe, text="  Material Manufacture Logo")
        self.ManufactureLogoImgLabel.pack(side=tk.LEFT)

        self.VIAProgramsframe = tk.Frame(self.master)
        self.VIAProgramsframe.pack(fill=tk.X)
        self.VIAProgramsCB = ttk.Combobox(self.VIAProgramsframe, validate='key', validatecommand=self.onUpdate)
        self.VIAProgramsCB.pack(side=tk.RIGHT)
        self.VIAProgramsCB['width']=30
        self.VIAProgramsCB.bind("<<ComboboxSelected>>", self.onUpdate)
        self.VIAProgramsLabel=tk.Label(self.VIAProgramsframe, text="  CSV Programs File")
        self.VIAProgramsLabel.pack(side=tk.LEFT)

        self.Manualsframe = tk.Frame(self.master)
        self.Manualsframe.pack(fill=tk.X)
        self.ManualsCB = ttk.Combobox(self.Manualsframe, validate='key', validatecommand=self.onUpdate)
        self.ManualsCB.pack(side=tk.RIGHT)
        self.ManualsCB['width']=30
        self.ManualsCB.bind("<<ComboboxSelected>>", self.onUpdate)
        self.ManualsLabel=tk.Label(self.Manualsframe, text="  User Manual")
        self.ManualsLabel.pack(side=tk.LEFT)

        self.FurnaceModelCB['values'], self.idFurnaceModelList, startvalue = self.combo_input("select  FurnaceModelName,  IDFurnaceModel from FurnaceModels")
        self.FurnaceModelCB.set(startvalue)
        self.onFurnaceModelSelect(0)
        
    def onFurnaceModelSelect(self, event):
        self.IsInsert.set(0)
        self.IsUpdate.set(0)
        self.ConfigurationCB.destroy()
        indexSelect=self.FurnaceModelCB.current()
        if(indexSelect >=0):
            self.idFurnaceModel=self.idFurnaceModelList[indexSelect];
            self.ConfigurationCB = ttk.Combobox(self.Configurationframe, validate='key', validatecommand=self.onInsert)
            self.ConfigurationCB.pack(side=tk.RIGHT)
            self.ConfigurationCB['width']=30
            self.ConfigurationCB.bind("<<ComboboxSelected>>", self.onConfigurationSelect)
            self.ConfigurationCB['values'], self.idConfigurationList, startvalue = self.combo_input("select NameConfiguration,  IDConfiguration from Configuration where IDFurnaceModel="+str(self.idFurnaceModel))
            self.ConfigurationCB.set(startvalue)
            self.onConfigurationSelect(0)
        
    def onConfigurationSelect(self, event):
        self.IsInsert.set(0)
        self.IsUpdate.set(0)
        self.bottomMessageLabel['text']=''
        self.buttonInsert['state']='disable'
        self.buttonUpdate['state']='disable'
        self.StartupImgCB.set('')
        self.ClientLogoImgCB.set('')
        self.ManufactureLogoImgCB.set('')
        self.VIAProgramsCB.set('')
        self.ManualsCB.set('')
        indexSelect=self.ConfigurationCB.current()
        if(indexSelect >=0):
            self.StartupImgCB['values'], self.idStartupImgList, startvalue = self.combo_input("select NameImage, IDImage from Images where IDTypeImage=1 ")
            self.StartupImgCB.set(startvalue)
            self.ClientLogoImgCB['values'], self.idClientLogoImgList, startvalue = self.combo_input("select NameImage, IDImage from Images where IDTypeImage=2 ")
            self.ClientLogoImgCB.set(startvalue)
            self.ManufactureLogoImgCB['values'], self.idManufactureLogoImgList, startvalue = self.combo_input("select NameImage, IDImage from Images where IDTypeImage=3 ")
            self.VIAProgramsCB['values'], self.idVIAProgramsList, startvalue = self.combo_input("select NameVIAProgram, IDVIAProgram from VIAPrograms ")
            self.VIAProgramsCB.set(startvalue)
            self.ManualsCB['values'], self.idManualsList, startvalue = self.combo_input("select NameManual, IDManual from Manuals ")
            self.ManualsCB.set(startvalue)
            self.idConfiguration=self.idConfigurationList[indexSelect];
        
            nameImage, self.idStartupImg=self.getDataConfiguration("select NameImage, cf.IDStartupImage from Configuration cf join Images im on cf.IDStartupImage=im.IDImage where cf.IDConfiguration=" + str(self.idConfiguration))
            self.StartupImgCB.set(nameImage)
    
            nameImage, self.idClientLogoImg=self.getDataConfiguration("select NameImage, cf.IDClientLogoImage from Configuration cf join Images im on cf.IDClientLogoImage=im.IDImage where cf.IDConfiguration=" + str(self.idConfiguration))
            self.ClientLogoImgCB.set(nameImage)
    
            nameImage, self.idManufactureLogoImg=self.getDataConfiguration("select NameImage, cf.IDManufactureLogoImage from Configuration cf join Images im on cf.IDManufactureLogoImage=im.IDImage where cf.IDConfiguration=" + str(self.idConfiguration))
            self.ManufactureLogoImgCB.set(nameImage)
    
            nameVIAProgram, self.idVIAProgram=self.getDataConfiguration("select NameVIAProgram, cf.IDVIAProgram from Configuration cf join VIAPrograms vp on cf.IDVIAProgram=vp.IDVIAProgram where cf.IDConfiguration=" + str(self.idConfiguration))
            self.VIAProgramsCB.set(nameVIAProgram)
    
            nameManual, self.idManual=self.getDataConfiguration("select NameManual, cf.IDManual from Configuration cf join Manuals m on cf.IDManual=m.IDManual where cf.IDConfiguration=" + str(self.idConfiguration))
            self.ManualsCB.set(nameManual)
            
    def deleteConfiguration(self):
        self.bottomMessageLabel['fg']='green'
        self.bottomMessageLabel['text']=''
        indexSelect=self.ConfigurationCB.current()
        if(indexSelect >=0):
            self.idConfiguration=self.idConfigurationList[indexSelect];
            try:
                cursor=self.dbconnection.cursor()
                                            
                sqlDeleteQuery="delete from  Configuration where IDConfiguration= "+str(self.idConfiguration);
                
                cursor.execute(sqlDeleteQuery)
                
                self.dbconnection.commit()
                self.bottomMessageLabel['text']='Configuration  was deleted successfully!!!\r\n'
                self.ConfigurationCB['values'], self.idConfigurationList, startvalue = self.combo_input("select NameConfiguration,  IDConfiguration from Configuration where IDFurnaceModel="+str(self.idFurnaceModel))
                self.ConfigurationCB.set(startvalue)
                self.onConfigurationSelect(0)
            except Exception as e:
                self.bottomMessageLabel['fg']='red'
                self.bottomMessageLabel['text']='Configuration deleting was fail!!! Exception is '+str(e)+'\r\n'
                
            self.master.focus()
            
        
    def getDataConfiguration(self, query):
        cursor=self.dbconnection.cursor()
        cursor.execute(query)
        
        nameobject=""
        idobject=-1
        
        for row in cursor.fetchall():
            nameobject=row[0]
            idobject=row[1]
        
        return nameobject, idobject
           
    def combo_input(self, query):
        cursor= self.dbconnection.cursor()
        
        cursor.execute(query)
    
        values = []
        keys = []
        
        first=True
        startvalue=''
        for row in cursor.fetchall():
            if(first == True):
                startvalue=row[0]
                first=False
            values.append(row[0])
            keys.append(row[1])
    
        return values, keys, startvalue  
    
    def onUpdate(self, *args):
        if(self.IsInsert.get()==0):
            self.IsUpdate.set(1)
            self.buttonUpdate['state']='normal'
            self.buttonInsert['state']='disable'
        
    def onInsert(self, *args):
        self.IsInsert.set(1)
        self.buttonInsert['state']='normal'
        self.buttonUpdate['state']= 'disable'

    def updateDataConfiguration(self):
        index_select=self.StartupImgCB.current()
        if(index_select>=0 and self.StartupImgCB.get()!=''):
            self.idStartupImg=self.idStartupImgList[index_select]
        else:
            self.idStartupImg=0
        
        index_select=self.ClientLogoImgCB.current()
        if(index_select >=0 and self.ClientLogoImgCB.get()!='' ):
            self.idClientLogoImg=self.idClientLogoImgList[index_select]
        else:
            self.idClientLogoImg=0
        
        index_select=self.ManufactureLogoImgCB.current()
        if(index_select >=0 and self.ManufactureLogoImgCB.get()!=''):
            self.idManufactureLogoImg=self.idManufactureLogoImgList[index_select]
        else:
            self.idManufactureLogoImg=0

        index_select=self.VIAProgramsCB.current()
        if(index_select >=0 and self.VIAProgramsCB.get()!=''):
            self.idVIAProgram=self.idVIAProgramsList[index_select]
        else:
            self.idVIAProgram=0

        index_select=self.ManualsCB.current()
        if(index_select >=0 and self.ManualsCB.get()!=''):
            self.idManual=self.idManualsList[index_select]
        else:
            self.idManual=0

    def insertConfiguration(self):
        self.bottomMessageLabel['fg']='green'
        self.bottomMessageLabel['text']='' 
        if(self.ConfigurationCB.get() == ''):
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='Enter name of Configuration please!!!'
            return 
        try:
            cursor=self.dbconnection.cursor()
            insert_query='insert into Configuration (IDFurnaceModel, NameConfiguration'
            insert_values='values(%s, %s'
            self.updateDataConfiguration()
            NameConfiguration=self.ConfigurationCB.get()
            values_tulpe=(self.idFurnaceModel, NameConfiguration)

            if(self.idStartupImg >0):
                insert_query=insert_query+', IDStartupImage'
                insert_values=insert_values+', %s'
                values_tulpe=values_tulpe+ (self.idStartupImg,)
            if(self.idClientLogoImg>0):
                insert_query=insert_query+', IDClientLogoImage'
                insert_values=insert_values+', %s'
                values_tulpe=values_tulpe+(self.idClientLogoImg,)
            if(self.idManufactureLogoImg>0):
                insert_query=insert_query+', IDManufactureLogoImage'
                insert_values=insert_values+', %s'
                values_tulpe=values_tulpe+(self.idManufactureLogoImg,)
            if(self.idManual>0):
                insert_query=insert_query+', IDManual'
                insert_values=insert_values+', %s'
                values_tulpe=values_tulpe+(self.idManual,)
            if(self.idVIAProgram>0):
                insert_query=insert_query+', IDVIAProgram'
                insert_values=insert_values+', %s'
                values_tulpe=values_tulpe+(self.idVIAProgram,)
            
            insert_query=insert_query+') '
            insert_values=insert_values+') '
            
            insert_query=insert_query+insert_values
                
            
            cursor.execute(insert_query, values_tulpe)
            
            self.dbconnection.commit()
            self.bottomMessageLabel['text']='Configuration was inserted successfully!!!!\r\n'
            self.ConfigurationCB['values'], self.idConfigurationList, startvalue = self.combo_input("select NameConfiguration,  IDConfiguration from Configuration where IDFurnaceModel="+str(self.idFurnaceModel))
            self.ConfigurationCB.set(NameConfiguration)
            self.onConfigurationSelect(0)
        except Exception as e:
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='Configuration saving was fail!!! Exception is'+str(e)+'\r\n'
        finally:
            cursor.close()
            self.buttonInsert['state']='disable'
            self.buttonUpdate['state']='disable'
            self.IsInsert.set(0)
            self.IsUpdate.set(0)
            
    def updateConfiguration(self):
        self.bottomMessageLabel['fg']='green'
        self.bottomMessageLabel['text']=''
        try:
            cursor=self.dbconnection.cursor()
            update_query='update Configuration set IDConfiguration=%s'
            
            self.updateDataConfiguration()
            values_tulpe=(self.idConfiguration,)
            
            if(self.idStartupImg >0):
                update_query=update_query+', IDStartupImage=%s'
                values_tulpe=values_tulpe+ (self.idStartupImg,)
            else:
                update_query=update_query+', IDStartupImage=NULL'
                
            if(self.idClientLogoImg>0):
                update_query=update_query+', IDClientLogoImage=%s'
                values_tulpe=values_tulpe+(self.idClientLogoImg,)
            else:
                update_query=update_query+', IDClientLogoImage=NULL'
                
            if(self.idManufactureLogoImg>0):
                update_query=update_query+', IDManufactureLogoImage=%s'
                values_tulpe=values_tulpe+(self.idManufactureLogoImg,)
            else:
                update_query=update_query+', IDManufactureLogoImage=NULL'
                
            if(self.idManual>0):
                update_query=update_query+', IDManual=%s'
                values_tulpe=values_tulpe+(self.idManual,)
            else:
                update_query=update_query+', IDManual=NULL'
            if(self.idVIAProgram>0):
                update_query=update_query+', IDVIAProgram=%s'
                values_tulpe=values_tulpe+(self.idVIAProgram,)
            else:
                update_query=update_query+', IDVIAProgram=NULL'
                
            update_query=update_query+' where IDConfiguration=%s'
            values_tulpe=values_tulpe+(self.idConfiguration,)
            
            cursor.execute(update_query, values_tulpe)
            
            self.dbconnection.commit()
            self.bottomMessageLabel['text']='Configuration was updated successfully!!!!\r\n'
        except Exception as e:
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='Configuration saving was fail!!! Exception is'+str(e)+'\r\n'
        finally:
            cursor.close()
            self.buttonInsert['state']='disable'
            self.buttonUpdate['state']='disable'
            self.IsInsert.set(0)
            self.IsUpdate.set(0)
    
    def closeWindow(self):
        self.master.destroy()