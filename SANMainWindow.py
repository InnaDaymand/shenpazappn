'''
Created on May 17, 2020

@author: Inna Daymand
'''
import tkinter as tk
from PIL import Image, ImageTk
from SANDatabaseManage import SANDatabaseManage
from SANSetupConfig import SANSetupConfig
from SANUploadConfiguration import SANUploadConfiguration
from SANSettings import SANSettings
import mysql.connector as mysql

class SANMainWindow(object):

    def __init__(self, master):
        self.master = master
        self.master.geometry("400x300")
        self.topframeBSD = tk.Frame(self.master)
        self.topframeBSD.pack(fill=tk.X)
        self.topBSDLabel=tk.Label(self.topframeBSD, text='בסד')
        self.topBSDLabel.pack(side=tk.RIGHT)
                 
        self.frame = tk.Frame(self.master)
        self.frame.pack(expand=True, fill=tk.BOTH, after=self.topframeBSD)
        load = Image.open("logoshenpaz.png")
        render = ImageTk.PhotoImage(load)
        labelLogo=tk.Label(self.frame, image=render)
        labelLogo.image=render
        labelLogo.pack(side=tk.TOP)
        self.connectMySQL()
        butSettings=tk.Button(self.frame, text='Setting Folder', command=self.Settings)
        butSettings.pack( after=labelLogo)
        butManageDB=tk.Button(self.frame, text='Manage Database', command=self.DBManage)
        butManageDB.pack( after= butSettings)
        butSetupConfiguration=tk.Button(self.frame, text='Setup Configuration', command=self.SetupConfig)
        butSetupConfiguration.pack( after=butManageDB)
        butUploadConfiguration=tk.Button(self.frame, text='Upload Configuration', command=self.UploadConfig)
        butUploadConfiguration.pack( after=butSetupConfiguration)
        
    def connectMySQL(self):
        self.dbconnection=mysql.connect(user='root', passwd='1234',
                              host='127.0.0.1',
                              db='shenpaz')
        cursor=self.dbconnection.cursor()
        cursor.execute("set @@global.max_allowed_packet=67108864")

    def DBManage(self):
        self.newWindow = tk.Toplevel(self.master)
        self.newWindow.geometry("600x400")
        self.app = SANDatabaseManage(self.newWindow, self.dbconnection)

    def SetupConfig(self):
        self.newWindow = tk.Toplevel(self.master)
        self.newWindow.geometry("600x400")
        self.app = SANSetupConfig(self.newWindow, self.dbconnection)
    
    def UploadConfig(self):
        self.newWindow = tk.Toplevel(self.master)
        self.newWindow.geometry("550x650")
        self.app = SANUploadConfiguration(self.newWindow, self.dbconnection)

    def Settings(self):
        self.newWindow = tk.Toplevel(self.master)
        self.newWindow.geometry("600x400")
        self.app = SANSettings(self.newWindow, self.dbconnection)
