'''
Created on 10 Jun 2020

@author: Inna Daymand
'''
import tkinter as tk
import tkinter.ttk as ttk
from tkinter import filedialog

class SANSettings(object):
    '''
    classdocs
    '''


    def __init__(self, master, dbconnect):
        self.master=master
        self.output_filename=''
        self.master.title("Application Global Settings")
        self.topframeBSD = tk.Frame(self.master)
        self.topframeBSD.pack(fill=tk.X)
        self.topBSDLabel=tk.Label(self.topframeBSD, text='בסד')
        self.topBSDLabel.pack(side=tk.RIGHT)
        self.topframe = tk.Frame(self.master)
        self.topframe.pack(fill=tk.X, after=self.topframeBSD)
                 
                 
        self.cellarframe=tk.Frame(self.master)
        self.cellarframe.pack(side=tk.BOTTOM)
        self.cellarframe.pack(fill=tk.X)
        self.cellarframe["height"]=10

        self.bottomframe=tk.Frame(self.master)
        self.bottomframe.pack(side=tk.BOTTOM)
        self.bottomframe.pack(fill=tk.X, after=self.cellarframe)
        self.bottomframe['height']=10
                
        self.bottomMessageframe=tk.Frame(self.master)
        self.bottomMessageframe.pack(side=tk.BOTTOM)
        self.bottomMessageframe.pack(fill=tk.X, after=self.bottomframe)
        self.bottomMessageLabel=tk.Label(self.bottomMessageframe)
        self.bottomMessageLabel.pack(fill=tk.X)

        self.butBack=tk.Button(self.topframe, text="Back", command=self.closeWindow)
        self.butBack.pack(side=tk.RIGHT)
        self.dbconnection=dbconnect
        self.Settings=APPSettings(self.dbconnection)
        self.Settings.getSettings()
        self.makeWindow()
    
    def closeWindow(self):
        self.master.destroy()

    def makeWindow(self):
        self.ReportFolderframe = tk.Frame(self.master)
        self.ReportFolderframe.pack(fill=tk.X)
        self.ReportFolderLabelValue = ttk.Label(self.ReportFolderframe)
        self.ReportFolderLabelValue['width']=60
        self.ReportFolderLabelValue.pack(side=tk.RIGHT, padx=10)
        self.ReportFolderLabelValue['text']=self.Settings.Settings['ReportDirectory']
        self.ReportFolderButton=tk.Button(self.ReportFolderframe, text=" Report folder select", command=self.selectReportFolder)
        self.ReportFolderButton.pack(side=tk.LEFT)

        self.PictireFolderframe = tk.Frame(self.master)
        self.PictireFolderframe.pack(fill=tk.X)
        self.PictureFolderLabelValue = ttk.Label(self.PictireFolderframe)
        self.PictureFolderLabelValue['width']=60
        self.PictureFolderLabelValue['text']=self.Settings.Settings['PictureDirectory']
        self.PictureFolderLabelValue.pack(side=tk.RIGHT, padx=10)
        self.PictureFolderButton=tk.Button(self.PictireFolderframe, text=" Report .jpeg model", command=self.selectPictureFolder)
        self.PictureFolderButton.pack(side=tk.LEFT)

        self.DBFolderframe = tk.Frame(self.master)
        self.DBFolderframe.pack(fill=tk.X)
        self.DBFolderLabelValue = ttk.Label(self.DBFolderframe)
        self.DBFolderLabelValue['width']=60
        self.DBFolderLabelValue['text']=self.Settings.Settings['DBDirectory']
        self.DBFolderLabelValue.pack(side=tk.RIGHT, padx=10)
        self.DBFolderButton=tk.Button(self.DBFolderframe, text=" DB folder select", command=self.selectDBFolder)
        self.DBFolderButton.pack(side=tk.LEFT)
    
        self.TargzFolderframe = tk.Frame(self.master)
        self.TargzFolderframe.pack(fill=tk.X)
        self.TargazFolderLabelValue = ttk.Label(self.TargzFolderframe)
        self.TargazFolderLabelValue['width']=60
        self.TargazFolderLabelValue['text']=self.Settings.Settings['TargzDirectory']
        self.TargazFolderLabelValue.pack(side=tk.RIGHT, padx=10)
        self.TargazFolderButton=tk.Button(self.TargzFolderframe, text=" Targz folder select", command=self.selectTargzFolder)
        self.TargazFolderButton.pack(side=tk.LEFT)
    
        self.UploadFolderframe = tk.Frame(self.master)
        self.UploadFolderframe.pack(fill=tk.X)
        self.UploadFolderLabelValue = ttk.Label(self.UploadFolderframe)
        self.UploadFolderLabelValue['width']=60
        self.UploadFolderLabelValue['text']=self.Settings.Settings['UploadTargzDirectory']
        self.UploadFolderLabelValue.pack(side=tk.RIGHT, padx=10)
        self.UploadFolderButton=tk.Button(self.UploadFolderframe, text="Upload Targz folder select", command=self.selectUploadTargzFolder)
        self.UploadFolderButton.pack(side=tk.LEFT)
    
        self.TargzFilenameframe = tk.Frame(self.master)
        self.TargzFilenameframe.pack(fill=tk.X)
        self.TargazFilenameLabelValue = ttk.Label(self.TargzFilenameframe)
        self.TargazFilenameLabelValue['width']=60
        self.TargazFilenameLabelValue['text']=self.Settings.Settings['TargzFilename']
        self.TargazFilenameLabelValue.pack(side=tk.RIGHT, padx=10)
        self.TargazFilenameButton=tk.Button(self.TargzFilenameframe, text=" Targz default file select", command=self.selectTargzfilename)
        self.TargazFilenameButton.pack(side=tk.LEFT)

    def updateSettingFolder(self, keywordSetting, informMessage, widget):
        self.Settings.Settings[keywordSetting]=filedialog.askdirectory()
        self.master.focus()
        if(self.Settings.Settings[keywordSetting] == ''):
            self.bottomMessageLabel['text']=informMessage
            widget['text']=''
            return
        self.updateSetting(keywordSetting, widget)
            
    def updateSettingFilename(self, keywordSetting, informMessage, widget):
        self.Settings.Settings[keywordSetting]=filedialog.askopenfilename()
        self.master.focus()
        if(self.Settings.Settings[keywordSetting] == ''):
            self.bottomMessageLabel['text']=informMessage
            widget['text']=''
            return
        self.updateSetting(keywordSetting, widget)

    def updateSetting(self, keywordSetting, widget):
        self.bottomMessageLabel['fg']='green'
        try:
            cursor=self.dbconnection.cursor()
            
            query='call sUpdateSettings ("'+keywordSetting+'", "'+self.Settings.Settings[keywordSetting]+'")'
            
            cursor.execute(query)
            self.dbconnection.commit()
            widget['text']=self.Settings.Settings[keywordSetting]
        except Exception as e:
            self.bottomMessageLabel['fg']='red'
            self.bottomMessageLabel['text']='Something is wrong, '+str(e)
        finally:
            cursor.close()
    
    def selectReportFolder(self):
        self.updateSettingFolder('ReportDirectory', 'Report directory is not selected!!! Please try again.', self.ReportFolderLabelValue)
        
    def selectPictureFolder(self):
        self.updateSettingFolder('PictureDirectory', 'Picture directory is not selected!!! Please try again.', self.PictureFolderLabelValue)

    def selectDBFolder(self):
        self.updateSettingFolder('DBDirectory', 'DB directory is not selected!!! Please try again.', self.DBFolderLabelValue)

    def selectTargzFolder(self):
        self.updateSettingFolder('TargzDirectory', 'Targz directory is not selected!!! Please try again.', self.TargazFolderLabelValue)

    def selectUploadTargzFolder(self):
        self.updateSettingFolder('UploadTargzDirectory', 'Upload Targz directory is not selected!!! Please try again.', self.UploadFolderLabelValue)

    def selectTargzfilename(self):
        self.updateSettingFilename('TargzFilename', 'Targz directory is not selected!!! Please try again.', self.TargazFilenameLabelValue)

class APPSettings (object):
    def __init__(self, dbconnect):
        self.dbconnection=dbconnect
        self.Settings=dict([])
        self.getSettings()
         
    def getSettings(self):
        self.Settings.setdefault('ReportDirectory', '')
        self.Settings.setdefault('PictureDirectory', '')
        self.Settings.setdefault('DBDirectory', '')
        self.Settings.setdefault('TargzDirectory', '')
        self.Settings.setdefault('UploadTargzDirectory', '')
        self.Settings.setdefault('TargzFilename', '')
        cursor=self.dbconnection.cursor()
        
        query='select NameSetting, ValueSetting from Settings'
        
        cursor.execute(query)
        
        for row in cursor.fetchall():
            self.Settings[row[0]]=row[1]
        cursor.close()
        
