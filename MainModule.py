'''
Created on May 13, 2020

@author: Inna Daymand
'''
import tkinter as tk
from SANMainWindow import SANMainWindow
    

if __name__ == '__main__':
    root = tk.Tk()
    root.title('Main App')
    app=SANMainWindow(root)
    root.mainloop()
